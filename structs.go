package sdch

import "time"

type Host struct {
	Host        string    `bson:"host"`
	LastGenTime time.Time `bson:"lastGenTime"`
}

type Page struct {
	Host string `bson:"host"`
	Key  string `bson:"key"`
	URI  string `bson:"URI"`
	//optional but strongly recommended fields
	Referer         string `bson:"referer,omitempty"`
	ContentType     string `bson:"contentType,omitempty"`
	ContentEncoding string `bson:"contentEncoding,omtiempty"`
	ContentLength   int32  `bson:"contentLength,omitempty"`
}
